
<!-- <div class="main-panel"> -->
  <div class="content-wrapper">

<div class="grid-margin stretch-card flex-column">

<div class="container text-center">
  <!-- ******************************************* CANVA ACTIVIDAD 3 ********************************************* -->
  <h3>TOTAL VISITS BY MONTH</h3>
  <h5>Activity 3</h5>
  <div class="row">


          <!-- diego -->


       

          <!-- Año 2023 -->
          <div class="col-md-3">
                <!-- aqui edito diego -->
                <div class="card" style="width: 100%;  border: 2px solid #000; border-radius: 10px;">
                  <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2023 -->
                    <div class="d-flex align-items-center justify-content-between justify-content-md-center justify-content-xl-between flex-wrap mb-4" style="background-color: #FF7456; padding: 15px;border: 2px; border-radius: 10px;">
                      <div>
                        <p class="mb-2 text-md-center text-lg-left">YEAR 2023</p>
                        <h3 class="mb-0"><?php echo $List2023; ?></h3>
                      </div>
                      <i class="far fa-calendar-alt icon-xl text-secondary"></i>
                    </div>
                    <!-- GRAFICA ESTADISTICA 2023 -->
                    <canvas id="lineChart2023" width="300" height="300"></canvas>
                  </div>
                </div>
          </div>

      </div>
  </div>
</div>

<!-- ******************************************* CANVA ACTIVIDAD 4 ********************************************* -->
<br>


<div class="container text-center">
  <h3><center>TOTAL NOTIFICATIONS</center></h3>
  <h5>Activity 4</h5>
  <div class="row">
    <!-- Año 2020 -->
    <div class="col-md-6">

      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card flex-grow-1" style="width: 100%; border: 2px solid #000; border-radius: 10px;">

        <div class="card-body">
        <!-- GRAFICA INDICADOR TOTAL 2020 -->
        <div class="card-body" style="background-color: #99FEF3; height: 90px; position: relative; border: 2px; border-radius: 10px;">
    <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
      <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
        <i class="fas fa-info-circle"></i>
      </a>
    </div>
    <div class="collapse" id="collapseExample">
      <div class="card card-body">
        Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
      </div>
    </div>
    <div class="text-container" style="margin-right: 30px; ">
      <p class="card-text">Total notifications by code</p>
    </div>
    <h5 class="card-title">
      <?php echo $totalNotificacionesTop10; ?>
    </h5>
  </div>
          <!-- GRAFICA ESTADISTICA 2020 -->
          <canvas id="bar1" width="200" height="200"></canvas>
        </div>
      </div>
    </div>

    <!-- Año 2021 -->
    <div class="col-md-6">
      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card flex-grow-1" style="width: 100%; border: 2px solid #000; border-radius: 10px;">
        <div class="card-body">
          <!-- GRAFICA INDICADOR TOTAL 2021 -->
          <div class="card-body" style="background-color: #99FEA1; height: 90px; position: relative; border: 2px; border-radius: 10px;">
    <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
      <a class="btn btn-primary" data-toggle="collapse" href="#collapseStatus" role="button" aria-expanded="false" aria-controls="collapseStatus">
        <i class="fas fa-info-circle"></i>
      </a>
    </div>
    <div class="collapse" id="collapseStatus">
      <div class="card card-body">
        Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
      </div>
    </div>
    <div class="text-container" style="margin-right: 30px;">
      <p class="card-text">Total Notifications by Status</p>
    </div>
    <h5 class="card-title">
      <?php echo $totalEstados; ?>
    </h5>
  </div>

          <!-- GRAFICA ESTADISTICA 2021 -->
          <canvas id="pie2" width="200" height="200"></canvas>
        </div>
      </div>
    </div>

  </div>
  <br>
  <div class="row">
    <!-- Año 2022 -->
    <div class="col-md-6">
      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card flex-grow-1" style="width: 100%;  border: 2px solid #000; border-radius: 10px;">
        <div class="card-body">
          <!-- GRAFICA INDICADOR TOTAL 2022 -->
          <div class="card-body" style="background-color: #D999FE; height: 90px; position: relative; border: 2px; border-radius: 10px;">
    <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
      <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">
        <i class="fas fa-info-circle"></i>
      </a>
    </div>
    <div class="collapse" id="collapseExample2">
      <div class="card card-body">
        Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
      </div>
    </div>
    <div class="text-container" style="margin-right: 30px;">
      <p class="card-text">Notifications by Type of Activity and Year</p>
    </div>
    <h5 class="card-title">
      <?php echo $totalNotificaciones; // Aquí debes obtener la variable $totalNotificaciones en tu controlador ?>
    </h5>
</div>

          <!-- GRAFICA ESTADISTICA 2022 -->
          <canvas id="line" width="200" height="200"></canvas>
        </div>
      </div>
    </div>

    <!-- Año 2023 -->
    <div class="col-md-6">
      <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
      <div class="card flex-grow-1" style="width: 100%;  border: 2px solid #000; border-radius: 10px;">
        <div class="card-body">
          <!-- GRAFICA INDICADOR TOTAL 2023 -->
          <div class="card-body" style="background-color: #FF7456; height: 90px; position: relative; border: 2px; border-radius: 10px;">
    <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            <i class="fas fa-info-circle"></i>
        </a>
    </div>
    <div class="collapse" id="collapseExample">
        <div class="card card-body">
            Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
        </div>
    </div>
    <div class="text-container" style="margin-right: 30px;">
        <p class="card-text">Total Notifications by State and Month</p>
    </div>
    <h5 class="card-title">
        <?php echo $totalSolicitudes; ?>
    </h5>
</div>

          <!-- GRAFICA ESTADISTICA 2023 -->
          <canvas id="pie" width="200" height="200"></canvas>
        </div>
      </div>
    </div>
  </div>
</div>
<!-- LIZ -->
<br>
<br>
<!-- ******************************************* CANVA ACTIVIDAD 5 ********************************************* -->
<!-- CINTIA -->



<br>
<!-- ******************************************* CANVA ACTIVIDAD 6 ********************************************* -->
<!-- ANGEL -->
<div class="container text-center" style="margin-top: 30px;">
  <h3><center>TOTAL NOTIFICATIONS, REQUESTS BY USERS</center></h3>
  <h5>Actividad 6</h5>
  <div class="row">
          <!-- NOTIFICACIONES Y SOLICITUDES TOTAL -->
          <div class="col-md-6">
              <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
              <div class="card" style="width: 100%; border: 2px solid #000; border-radius: 10px;">
                  <div class="card-body">
                      <!-- GRAFICA INDICADOR TOTAL 2020 -->
                      <div class="card-body" style="background-color: #99FEF3; height: 190px; position: relative;border: 2px; border-radius: 10px;">
      <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
          <i class="fas fa-info-circle"></i>
        </a>
      </div>
      <div class="collapse" id="collapseExample">
        <div class="card card-body">
          Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
        </div>
      </div>
      <div class="text-container" style="margin-right: 30px;">
        <h6 class="card-text">TOTAL Notifications AND Request By USER</h6>
        <p>1.- Total of Notifications:</p>
        <h6 class="card-title">
          <?php echo $totalNotifications; ?>
        </h6>
        <p>2.- Total of Requests:</p>
        <h6 class="card-title">
          <?php echo $totalSolicitudes; ?>
        </h6>
      </div>
    </div>

                      <!-- GRAFICA ESTADISTICA 2020 -->
                      <canvas id="barras6" width="200" height="200"></canvas>
                  </div>
              </div>
          </div>

          <!-- NOTIFICACIONES RECHAZADAS Y SOLICITUDES APROBADAS-->
          <div class="col-md-6">
              <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
              <div class="card" style="width: 100%;  border: 2px solid #000; border-radius: 10px;">
                  <div class="card-body">
                      <!-- GRAFICA INDICADOR TOTAL 2021 -->
                      <div class="card-body" style="background-color: #99FEA1; height: 190px; position: relative;border: 2px; border-radius: 10px;">
    <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
      <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample2" role="button" aria-expanded="false" aria-controls="collapseExample2">
        <i class="fas fa-info-circle"></i>
      </a>
    </div>
    <div class="collapse" id="collapseExample2">
      <div class="card card-body">
        <!-- Contenido del colapso -->
        <p>1.- Total of Notifications in 2022:</p>
        <h6 class="card-title">
          <?php echo $totalNotificaciones_62; ?>
        </h6>
        <p>2.- Total of Request in 2022:</p>
        <h6 class="card-title">
          <?php echo $totalSolicitudes_62; ?>
        </h6>
      </div>
    </div>
    <div class="text-container" style="margin-right: 30px;">
      <h6 class="card-text">TOTAL Requests & Notifications in 2022 by USER</h6>
    </div>
  </div>

                      <!-- GRAFICA ESTADISTICA 2021 -->
                      <canvas id="barras7" width="200" height="200"></canvas>
                  </div>
              </div>
          </div>
      </div>
  </div><!-- FIN CLASE -->
</div>

<br>
<div class="container text-center">
  <div class="row">
        <!-- NOTIFICACIONES Y SOLICITUDES TOTAL -->
        <div class="col-md-6">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="width: 100%;  border: 2px solid #000; border-radius: 10px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2020 -->
                    <?php $uniqueId = uniqid(); ?>

  <div class="card-body" style="background-color: #99FEF3; height: 190px; position: relative; border: 2px; border-radius: 10px;">
      <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
          <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample_<?php echo $uniqueId; ?>" role="button" aria-expanded="false" aria-controls="collapseExample_<?php echo $uniqueId; ?>">
              <i class="fas fa-info-circle"></i>
          </a>
      </div>
      <div class="collapse" id="collapseExample_<?php echo $uniqueId; ?>">
          <div class="card card-body">
              Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
          </div>
      </div>
      <div class="text-container" style="margin-right: 30px;">
          <h6 class="card-text">TOTAL Notifications & Requests IN 2023</h6>
          <p>1.- Total of Notifications in 2023:</p>
          <h6 class="card-title">
              <?php echo $totalNotificaciones_63; ?>
          </h6>
          <p>2.- Total of Requests in 2023:</p>
          <h6 class="card-title">
              <?php echo $totalSolicitudes_63; ?>
          </h6>
      </div>
  </div>


                    <!-- GRAFICA ESTADISTICA 2020 -->
                    <canvas id="barras_63" width="300" height="300"></canvas>
                </div>
            </div>
        </div>

        <!-- NOTIFICACIONES RECHAZADAS Y SOLICITUDES APROBADAS-->
        <div class="col-md-6">
            <!-- AJUSTANDO DIMENSION DE TARJETA PARA GENERAR KPI MEDIANO -->
            <div class="card" style="width: 100%;  border: 2px solid #000; border-radius: 10px;">
                <div class="card-body">
                    <!-- GRAFICA INDICADOR TOTAL 2021 -->
                    <div class="card-body" style="background-color: #99FEA1; height: 190px; position: relative; border: 2px; border-radius: 10px;">
    <!-- Agregado el botón de información -->
    <div class="d-flex justify-content-end align-items-start" style="position: absolute; top: 0; right: 0;">
        <a class="btn btn-primary" data-toggle="collapse" href="#collapseExample" role="button" aria-expanded="false" aria-controls="collapseExample">
            <i class="fas fa-info-circle"></i>
        </a>
    </div>
    <!-- Contenido principal -->
    <h6 class="card-text">TOTAL Notifications & Requests APPROVED per USER</h6>
    <p>1.- Total of Notifications in Comercio1:</p>
    <h6 class="card-title">
        <?php echo $totalNotificaciones_64; ?>
    </h6>
    <p>2.- Total of Notifications in Comercio1:</p>
    <h6 class="card-title">
        <?php echo $totalSolicitudes_64; ?>
    </h6>
    <!-- Contenido adicional colapsable con la clase "show" para mostrarlo inicialmente -->
    <div class="collapse show" id="collapseExample">
        <div class="card card-body">
            <!-- Agrega aquí el contenido adicional que deseas mostrar -->
            Some placeholder content for the collapse component. This panel is hidden by default but revealed when the user activates the relevant trigger.
        </div>
    </div>
</div>

                    <!-- GRAFICA ESTADISTICA 2021 -->
                    <canvas id="barras_64" width="200" height="200"></canvas>
                </div>
            </div>
        </div>
    </div>
</div>
</div>

<br>

<!-- FIN CLASE -->

<!-- ****************************************** SCRIPT ACTIVIDAD 3 *******************************************-->
<!-- 2020 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2020): ?>

         <?php foreach ($getByVisit2020 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2020',
       data: [
         <?php if ($getByVisit2020): ?>

           <?php foreach ($getByVisit2020 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2020').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- 2021 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2021): ?>

         <?php foreach ($getByVisit2021 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2021',
       data: [
         <?php if ($getByVisit2021): ?>

           <?php foreach ($getByVisit2021 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2021').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- 2022 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2022): ?>

         <?php foreach ($getByVisit2022 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2022',
       data: [
         <?php if ($getByVisit2022): ?>

           <?php foreach ($getByVisit2022 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2022').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- 2023 -->
<script type="text/javascript">
  // Datos de ejemplo
   var datos = {
     labels: [

       <?php if ($getByVisit2023): ?>

         <?php foreach ($getByVisit2023 as $contador): ?>
           '<?php echo $contador->Months; ?>',
         <?php endforeach; ?>

       <?php endif; ?>

     ],
     datasets: [{
       label: 'Total Visits 2023',
       data: [
         <?php if ($getByVisit2023): ?>

           <?php foreach ($getByVisit2023 as $contador): ?>
             <?php echo $contador->Total; ?>,
           <?php endforeach; ?>

         <?php endif; ?>

       ], // Color fondo de las barras
       backgroundColor: [
         'rgba(255, 99, 132, 0.6)', // Color de la primera barra
         'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
         'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       //Color borde de barras
       borderColor: [
         'rgba(255, 99, 132, 1)',
         'rgba(54, 162, 235, 1)',
         'rgba(255, 206, 86, 1)',
         'rgba(153, 254, 161)',
         'rgba(217, 153, 254)',
         'rgba(198, 177, 122)'
       ],
       borderWidth: 1
     }]
   };

   // Opciones de configuracion
   var opciones = {
     scales: {
       y: {
         beginAtZero: true
       }
     }
   };

   // Obtener el contexto del lienzo
   var contexto = document.getElementById('lineChart2023').getContext('2d');

   // Crear el grafico de barras
   var graficoDeBarras = new Chart(contexto, {
     type: 'line',
     data: datos,
     options: opciones
   });
</script>


<!-- ****************************************** SCRIPT ACTIVIDAD 4 *******************************************-->
<!-- LIZ -->
<script type="text/javascript">
  var datos = {
      labels: [
        <?php if ($bomberosByNotificacion): ?>
          <?php foreach ($bomberosByNotificacion as $bombero): ?>
            '<?php echo $bombero-> codigo_sol; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ],
      datasets: [{
        label: 'Datos de ejemplo',
        data: [
          <?php if ($bomberosByNotificacion): ?>
            <?php foreach ($bomberosByNotificacion as $bombero): ?>
              '<?php echo $bombero-> total_notificaciones; ?>',
            <?php endforeach; ?>
          <?php endif; ?>
        ], // Valores de las barras
        backgroundColor: [
          'rgba(255, 99, 132, 0.6)', // Color de la primera barra
          'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
          'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)'
        ],
        borderWidth: 1
      }]
    };

    // Opciones de configuraci�n
    var opciones = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('bar1').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
      type: 'bar',
      data: datos,
      options: opciones
    });
</script>

<script type="text/javascript">
  var datos = {
      labels: [
        <?php if ($bomberosByState): ?>
          <?php foreach ($bomberosByState as $bombero): ?>
            '<?php echo $bombero-> estado_sol; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ],
      datasets: [{
        label: 'Datos de ejemplo',
        data: [
          <?php if ($bomberosByState): ?>
            <?php foreach ($bomberosByState as $bombero): ?>
              '<?php echo $bombero-> porcentaje; ?>',
            <?php endforeach; ?>
          <?php endif; ?>
        ], // Valores de las barras
        backgroundColor: [
          'rgba(255, 99, 132, 0.6)', // Color de la primera barra
          'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
          'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)'
        ],
        borderWidth: 1
      }]
    };

    // Opciones de configuraci�n
    var opciones = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('pie2').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
      type: 'doughnut',
      data: datos,
      options: opciones
    });
</script>

<script type="text/javascript">

  var datos = {
      labels: [
        <?php if ($bomberosByActividad): ?>
          <?php foreach ($bomberosByActividad as $bombero): ?>
            '<?php echo $bombero-> actividad_sol; ?>',
          <?php endforeach; ?>
        <?php endif; ?>
      ],
      datasets: [{
        label: 'Datos de ejemplo',
        data: [
          <?php if ($bomberosByActividad): ?>
            <?php foreach ($bomberosByActividad as $bombero): ?>
              '<?php echo $bombero-> count_notificaciones; ?>',
            <?php endforeach; ?>
          <?php endif; ?>
        ], // Valores de las barras
        backgroundColor: [
          'rgba(255, 99, 132, 0.6)', // Color de la primera barra
          'rgba(54, 156, 235, 0.6)', // Color de la segunda barra
          'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
        ],
        borderColor: [
          'rgba(255, 99, 132, 1)',
          'rgba(54, 162, 235, 1)',
          'rgba(255, 206, 86, 1)'
        ],
        borderWidth: 1
      }]
    };

    // Opciones de configuraci�n
    var opciones = {
      scales: {
        y: {
          beginAtZero: true
        }
      }
    };

    // Obtener el contexto del lienzo
    var contexto = document.getElementById('line').getContext('2d');

    // Crear el gr�fico de barras
    var graficoDeBarras = new Chart(contexto, {
      type: 'line',
      data: datos,
      options: opciones
    });
</script>

<script type="text/javascript">
  var datos = {
      labels: [
          <?php if ($bomberosByMeses): ?>
              <?php foreach ($bomberosByMeses as $bombero): ?>
                  '<?php echo $bombero->anio; ?>',
              <?php endforeach; ?>
          <?php endif; ?>
      ],
      datasets: [{
          label: 'Datos de ejemplo',
          data: [
              <?php if ($bomberosByMeses): ?>
                  <?php foreach ($bomberosByMeses as $bombero): ?>
                      '<?php echo $bombero->porcentaje; ?>',
                  <?php endforeach; ?>
              <?php endif; ?>
          ],
          backgroundColor: [
              'rgba(255, 99, 132, 0.6)', // Color de la primera barra
              'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
              'rgba(255, 206, 86, 0.6)', // Color de la tercera barra
              'rgba(75, 192, 192, 0.6)', // Color de la cuarta barra
              'rgba(153, 102, 255, 0.6)', // Color de la quinta barra
              'rgba(255, 159, 64, 0.6)' // Color de la sexta barra
          ],
          borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(75, 192, 192, 1)',
              'rgba(153, 102, 255, 1)',
              'rgba(255, 159, 64, 1)'
          ],
          borderWidth: 1
      }]
  };

  // Opciones de configuración
  var opciones = {
      scales: {
          y: {
              beginAtZero: true
          }
      }
  };

  // Obtener el contexto del lienzo
  var contexto = document.getElementById('pie').getContext('2d');

  // Crear el gráfico de barras
  var graficoDeBarras = new Chart(contexto, {
      type: 'pie',
      data: datos,
      options: opciones
  });
</script>


<!-- ****************************************** SCRIPT ACTIVIDAD 5 *******************************************-->
<!-- CINTIA -->
<script type="text/javascript">
        // Gráfico 1: Cantidad de mensajes por año
        var datosMensajesPorAnio = {
            labels: [
                <?php foreach ($cantidadMensajesPorAnio as $dato) : ?>
                    '<?php echo $dato->anio_lot; ?>',
                <?php endforeach; ?>
            ],
            datasets: [{
                label: 'number of calls per year',
                data: [
                    <?php foreach ($cantidadMensajesPorAnio as $dato) : ?>
                        <?php echo $dato->cantidad_mensajes; ?>,
                    <?php endforeach; ?>
                ],
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)',
                  'rgba(255, 206, 86, 0.6)',
                  'rgba(75, 192, 192, 0.6)',
                ],
                borderColor: 'rgba(255, 99, 132, 1)',
                borderWidth: 1
            }]
        };

        var ctxMensajesPorAnio = document.getElementById('graficoMensajesPorAnio').getContext('2d');
        new Chart(ctxMensajesPorAnio, {
            type: 'bar',
            data: datosMensajesPorAnio,
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        // Gráfico 2: Cantidad de mensajes por año y mes
        var datosMensajesPorAnioYMes = {
            labels: [
                <?php foreach ($mensajesPorAnioYMes as $dato) : ?>
                    '<?php echo $dato->anio; ?>-<?php echo $dato->mes; ?>',
                <?php endforeach; ?>
            ],
            datasets: [{
                label: 'number of calls per year and per month',
                data: [
                    <?php foreach ($mensajesPorAnioYMes as $dato) : ?>
                        <?php echo $dato->cantidad_mensajes; ?>,
                    <?php endforeach; ?>
                ],
                backgroundColor: [
                  'rgba(255, 99, 132, 0.6)',
                  'rgba(54, 162, 235, 0.6)',
                  'rgba(255, 206, 86, 0.6)',
                  'rgba(75, 192, 192, 0.6)',
                ],
                borderColor: 'rgba(54, 162, 235, 1)',
                borderWidth: 1
            }]
        };

        var ctxMensajesPorAnioYMes = document.getElementById('graficoMensajesPorAnioYMes').getContext('2d');
        new Chart(ctxMensajesPorAnioYMes, {
            type: 'bar',
            data: datosMensajesPorAnioYMes,
            options: {
                scales: {
                    y: {
                        beginAtZero: true
                    }
                }
            }
        });

        // Gráfico 3: Cantidad de archivos por tipo
        var datosCantidadArchivosPorTipo = {
            labels: [
                'Files',
                'Gallery'
            ],
            datasets: [{
                label: 'Cantidad de archivos por tipo',
                data: [
                    <?php foreach ($cantidadArchivosPorTipo as $dato) : ?>
                        <?php echo $dato->cantidad_archivos; ?>,
                    <?php endforeach; ?>
                ],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.6)',
                    'rgba(54, 162, 235, 0.6)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)'
                ],
                borderWidth: 1
            }]
        };

        var ctxCantidadArchivosPorTipo = document.getElementById('graficoCantidadArchivosPorTipo').getContext('2d');
        new Chart(ctxCantidadArchivosPorTipo, {
            type: 'doughnut',
            data: datosCantidadArchivosPorTipo,
            options: {}
        });
    </script>

<!-- ****************************************** SCRIPT ACTIVIDAD 6 *******************************************-->
<!-- ANGEL -->
<!-- Funcion 6.1: KPI_61  -->
<script type="text/javascript">
    var datos = {
        labels: [
            <?php foreach ($dataForKPI6 as $tblsUsuNotiSoli): ?>
                '<?php echo $tblsUsuNotiSoli->NombreUsuario; ?>',
            <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total of Notifications',
            data: [
                <?php foreach ($dataForKPI6 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalNotificaciones; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(255, 99, 132, 0.6)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
        }, {
            label: 'Total of Requests',
            data: [

                <?php foreach ($dataForKPI6 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalSolicitudes; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(54, 162, 235, 0.6)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1
        }]
    };

    var opciones = {
      scales: {
        x: {
          stacked: true
        }


      }
    };

    var contexto = document.getElementById('barras6').getContext('2d');

    var barras6 = new Chart(contexto, {
        type: 'bar', // Cambié 'line' a 'bar' para barras en lugar de línea
        data: datos,
        options: opciones
    });
</script>

<!-- Funcion 6.2: KPI_62  -->
<script type="text/javascript">
    var datos = {
        labels: [
            <?php foreach ($dataForKPI_62 as $tblsUsuNotiSoli): ?>
                '<?php echo $tblsUsuNotiSoli->NombreUsuario; ?>',
            <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total de  Request Approved',
            data: [
                <?php foreach ($dataForKPI_62 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalSolicitudes2022; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: [
              'rgba(255, 99, 132, 0.6)', // Color de la primera barra
              'rgba(54, 162, 235, 0.6)', // Color de la segunda barra
              'rgba(255, 206, 86, 0.6)',  // Color de la tercera barra
              'rgba(153, 254, 161)',
              'rgba(217, 153, 254)',
              'rgba(198, 177, 122)'
            ],
             //Color borde de barras
            borderColor: [
              'rgba(255, 99, 132, 1)',
              'rgba(54, 162, 235, 1)',
              'rgba(255, 206, 86, 1)',
              'rgba(153, 254, 161)',
              'rgba(217, 153, 254)',
              'rgba(198, 177, 122)'
            ],
            borderWidth: 1
        }, {
            label: 'Total de Request Cancelled',
            data: [
                <?php foreach ($dataForKPI_62 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalNotificaciones2022; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(54, 162, 235, 0.6)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1
        }]
    };

    var opciones = {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    };

    var contexto = document.getElementById('barras7').getContext('2d');

    var barras6 = new Chart(contexto, {
        type: 'bar', // Cambié 'line' a 'bar' para barras en lugar de línea
        data: datos,
        options: opciones
    });
</script>

<!-- Funcion 6.3: KPI_63  -->
<script type="text/javascript">
    var datos = {
        labels: [
            <?php foreach ($dataForKPI_63 as $tblsUsuNotiSoli): ?>
                '<?php echo $tblsUsuNotiSoli->NombreUsuario; ?>',
            <?php endforeach; ?>
        ],
        datasets: [{
            label: 'Total of Requests in 2023',
            data: [
                <?php foreach ($dataForKPI_63 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalSolicitudes2023; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(255, 99, 132, 0.6)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
        }, {
            label: 'Total of Notifications in 2023',
            data: [
                <?php foreach ($dataForKPI_63 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalNotificaciones2023; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(54, 162, 235, 0.6)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1
        }]
    };

    var opciones = {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    };

    var contexto = document.getElementById('barras_63').getContext('2d');

    var barras6 = new Chart(contexto, {
        type: 'bar', // Cambié 'line' a 'bar' para barras en lugar de línea
        data: datos,
        options: opciones
    });
</script>

<!-- Funcion 6.4: KPI_64  -->
<script type="text/javascript">
    var datos = {
        labels: [
            <?php foreach ($dataForKPI_64 as $tblsUsuNotiSoli): ?>
                '<?php echo $tblsUsuNotiSoli->NombreUsuario; ?> <?php echo $tblsUsuNotiSoli->Actividad; ?>',
            <?php endforeach; ?>

        ],
        datasets: [{
            label: 'Total de Solicitudes 2023',
            data: [
                <?php foreach ($dataForKPI_64 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalSolicitudes; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(255, 99, 132, 0.6)',
            borderColor: 'rgba(255, 99, 132, 1)',
            borderWidth: 1
        }, {
            label: 'Total de Notificaciones 2023',
            data: [
                <?php foreach ($dataForKPI_64 as $tblsUsuNotiSoli): ?>
                    <?php echo $tblsUsuNotiSoli->TotalNotificaciones; ?>,
                <?php endforeach; ?>
            ],
            backgroundColor: 'rgba(54, 162, 235, 0.6)',
            borderColor: 'rgba(54, 162, 235, 1)',
            borderWidth: 1
        }]
    };

    var opciones = {
      scales: {
        x: {
          stacked: true
        },
        y: {
          stacked: true
        }
      }
    };

    var contexto = document.getElementById('barras_64').getContext('2d');

    var barras6 = new Chart(contexto, {
        type: 'bar', // Cambié 'line' a 'bar' para barras en lugar de línea
        data: datos,
        options: opciones
    });
</script>
